import './App.css';
import React, { useState, useEffect } from 'react';


import Main from './components/Main';
function App() {
  const [theme, setTheme] = useState('dark');

  useEffect(() => {
    if (theme === 'dark') {
      document.documentElement.classList.add('dark');
    } else {
      document.documentElement.classList.remove('dark');
    }
  }, [theme]);

  const handlerTheme = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark');
  };

  return (
    <>
      <div
      
        className=" font-nunito lg:overflow-hidden overflow-auto h-screen  dark:bg-[#404258] bg-[#E0F0EA] "
      >
        <Main handlerTheme={handlerTheme} theme={theme} />
      </div>
    </>
  );
}

export default App;
