import React from 'react';
import { motion } from 'framer-motion';
import { BsArrowRightCircle, BsArrowLeftCircle } from 'react-icons/bs';

const ForcasrScroll = () => {
  const sliderRight = () => {
    let slider = document.getElementById('slider');
    slider.scrollLeft = slider.scrollLeft - 100;
  };
  const sliderLeft = () => {
    let slider = document.getElementById('slider');
    slider.scrollLeft = slider.scrollLeft + 100;
  };
  return (
    <div
      className="dark:text-slate-400 text-slate-700 mt-3  
          flex justify-center cursor-pointer"
    >
      <motion.button
        whileHover={{ scale: 1.2 }}
        whileTap={{ scale: 0.9 }}
        transition={{ type: 'spring', stiffness: 400, damping: 17 }}
      >
        <BsArrowLeftCircle onClick={sliderRight} size={25} className="mr-5" />
      </motion.button>
      <motion.button
        whileHover={{ scale: 1.2 }}
        whileTap={{ scale: 0.9 }}
        transition={{ type: 'spring', stiffness: 400, damping: 17 }}
      >
        <BsArrowRightCircle onClick={sliderLeft} size={25} />
      </motion.button>
    </div>
  );
};

export default ForcasrScroll;
