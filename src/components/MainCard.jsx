import React from 'react';
import Svg from './Svg';
import { motion } from 'framer-motion';

const MainCard = ({ weather }) => {
  return (
    <>
      {typeof weather.main != 'undefined' ? (
        <motion.div
          key={weather}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{
            duration: 0.4,
          }}
          className="lg:w-[35%] mt-[-0.5rem]  w-full lg:mx-auto md:justify-center "
        >
          <div
            className=" h-[8rem] flex p-4   mx-4  lg:mx-0
              lg:rounded-tl-[4rem] lg:rounded-br-[4rem] lg:rounded-tr-[1rem] lg:rounded-bl-[1rem] 
              rounded-lg
              border-slate-400 border-[1px] backdrop-opacity-10 backdrop-invert bg-white/30
              dark:backdrop-opacity-10 dark:backdrop-invert dark:bg-white/15
           "
          >
            <div className="lg:my-auto mt-[-1.2rem] ">
              <Svg weather={weather} />
            </div>
            <p className=" lg:text-xl grid lg:mt-3 mt-0 mx-auto md:text-3xl text-4xl font-semibold font-nunito text-gray-800 dark:text-gray-800">
              {weather?.name},{weather.sys.country}
              <h5 className="lg:text-lg lg:mt-0 grid mx-auto md:text-xl text-2xl text-gray-800 dark:text-gray-700">
                {weather.weather[0].main}
                <h6 className="lg:text-sm mx-auto">
                  {Math.round(weather.main.temp)}
                  °С
                </h6>
              </h5>
            </p>
          </div>
        </motion.div>
      ) : (
        ''
      )}
    </>
  );
};

export default MainCard;
