import React from 'react';
import { motion } from 'framer-motion';

import SvgForcast from './SvgForcast';

const ForcastCards = ({ forcs }) => {

  

  return (
    <>
      {typeof forcs != 'undefined' ? (
        <div className="px-3 snap-start group   ">
          <motion.div
            key={forcs}
            whileHover={{ width: '12rem', animationDelay: 0.8, duration: 0.5 }}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{
              duration: 0.3,
            }}
            className=" h-48 lg:w-20 grid gap-3 w-20 hover:border-sky-600
             lg:rounded-tl-[2.5rem] lg:rounded-br-[2.5rem] lg:rounded-tr-[0.5rem] 
             lg:rounded-bl-[0.5rem] rounded-xl lg:group-hover:w-20 group-hover:w-[12rem]  lg:transition-none group-hover:ease-in-out group-hover:duration-500
             border-[1px] border-slate-500 backdrop-opacity-10 backdrop-invert bg-white/30
             "
          >
            <p className="text-center font-semibold text-gray-800 dark:text-gray-700 text-xl mt-5">
              {Math.round(forcs.main.temp)}°С
            </p>

            <span className="mx-auto">
              <SvgForcast forcs={forcs} />
            </span>
            <p className="text-center text-gray-800 dark:text-gray-800 font-semibold">
              {forcs?.weather[0]?.main}
            </p>
            <p
              className="text-gray-800 group-out-of-range:hidden
              text-center group-hover:visible invisible
                font-semibold  mb-5 group-hover:ease-in group-hover:delay-300"
            >
              {forcs.dt_txt}
            </p>
          </motion.div>
        </div>
      ) : (
        ''
      )}
    </>
  );
};

export default ForcastCards;
