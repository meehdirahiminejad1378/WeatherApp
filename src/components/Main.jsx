import React, { useState, useEffect } from 'react';
import ForcastCards from './ForcastCards';
import { BiSearch, BiMoon } from 'react-icons/bi';
import { BsSun } from 'react-icons/bs';
import MainCard from './MainCard';
import { BsArrowDownCircle } from 'react-icons/bs';
import axios from 'axios';
import { motion } from 'framer-motion';
import ForcasrScroll from './ForcasrScroll';

const Main = ({ handlerTheme, theme }) => {
  const [input, setInput] = useState('');
  const [weather, setWeather] = useState({});
  const [forcast, setForcast] = useState([]);
  const [scroll, setScroll] = useState(false);

  const api = {
    apiKey: process.env.REACT_APP_KEY,
    weather: 'https://api.openweathermap.org/data/2.5/weather?q=',
    forcast_api: `https://api.openweathermap.org/data/2.5/forecast?id=`,
  };

  const searchClick = (e) => {
    e.preventDefault();
    const url = `${api.weather}${input}&units=metric&appid=${api.apiKey}`;

    try {
      if (input.trim() === '') {
        alert('Enter city ...');
      } else {
        const fetchWeather = async () => {
          const response = await axios.get(url);
          setWeather(response.data);
        };

        fetchWeather();
        setInput('');
      }
    } catch (err) {
      return err;
    }
  };

  const forcastHandler = (e) => {
    e.preventDefault();
    const url_forcast = `${api.forcast_api}${weather.id}&units=metric&appid=${api.apiKey}`;
    try {
      const fetchForcast = async () => {
        const response = await axios.get(url_forcast);
        setForcast(response.data.list);
        setScroll(true);
      };
      fetchForcast();
    } catch (err) {
      return err;
    }
  };

  let forcasts = [];

  for (let i = 0; i <= 15; i++) {
    forcasts.push(forcast[i]);
  }

  var today = new Date();
  var time =
    (today.getHours() > 12 ? today.getHours() : '0' + today.getHours()) +
    ':' +
    (today.getMinutes() > 9 ? today.getMinutes() : '0' + today.getMinutes());

  return (
    <>
      <motion.div
        key={theme}
        initial={{ opacity: 0.4 }}
        animate={{ opacity: 1 }}
        transition={{
          duration: 0.6,
        }}
        className=" bg-[#F1EDFF] mx-auto dark:bg-[#414756]   
        lg:mt-5 mt-0 drop-shadow-2xl lg:w-[50%]  w-[100%] 
         dark:shadow-lg dark:lg:shadow-sky-600/50 text-sky-600/10 
          lg:rounded-tl-[8rem] lg:rounded-br-[8rem] lg:rounded-tr-[1.5rem]
           lg:rounded-bl-[1.5rem]  lg:h-[94%] lg"
      >
        <div className="flex justify-center  w-full">
          <div className=" flex justify-center mt-5 md:w-[40%]  w-full px-8 lg:px-0 my-auto ">
            <input
              className="placeholder:italic placeholder:text-slate-900 block placeholder:text-lg
               h-12  w-full border drop-shadow-lg rounded-lg 
               lg:rounded-tl-[1.2rem] lg:rounded-br-[1.2rem] lg:rounded-tr-[0.5rem] lg:rounded-bl-[0.5rem]
               py-2 pl-3 pr-3 shadow-sm 
               text-slate-800 font-semibold focus:outline-none dark:text-slate-200
               backdrop-opacity-10 border-slate-300 
               sm:text-md backdrop-invert bg-white/30 dark:placeholder:text-slate-200
               dark:border-slate-600 dark:focus:border-[#e0f0ea]"
              placeholder="Search..."
              type="text"
              name="search"
              value={input}
              onChange={(e) => setInput(e.target.value)}
            />
            <motion.button
              whileHover={{ scale: 1.2 }}
              onClick={searchClick}
              whileTap={{ scale: 1.2 }}
              transition={{ type: 'spring', stiffness: 400, damping: 17 }}
              className="inline ml-[-2.5rem]  z-20 dark:text-white/50 text-slate-700  "
            >
              <BiSearch size={35} />
            </motion.button>
          </div>
        </div>

        <div className="pl-10 lg:mt-0 mt-5 ">
          <motion.button
            whileHover={{ scale: 1.2 }}
            whileTap={{ scale: 0.9 }}
            transition={{ type: 'spring', stiffness: 400, damping: 17 }}
            className="text-gray-700 dark:text-gray-400"
            onClick={handlerTheme}
          >
            {theme === 'dark' ? (
              <BsSun size={28} className=" text-sky-600/50  " />
            ) : (
              <BiMoon size={28} className="text-gray-700 " />
            )}
          </motion.button>
        </div>
        <div className=" mt-4 lg:flex  grid  gap-4   w-full py-4 ">
          <div className="text-gray-800  lg:mt-6 mt-0  lg:mr-[4rem]"></div>
          <p
            className=" font-normal text-6xl flex justify-center lg:mt-5 lg:mx font-nunito
             text-gray-800 dark:text-gray-400 "
          >
            {time}
          </p>
          <MainCard weather={weather} />
        </div>

        {/* Foracst card */}

        <div className="lg:flex flex justify-center mt-4">
          <span className="ml-2 my-auto font-semibold text-gray-800 dark:text-gray-400">
            Show Forcasts
          </span>
          <motion.button
            whileHover={{ scale: 1.2 }}
            whileTap={{ scale: 0.9 }}
            transition={{ type: 'spring', stiffness: 400, damping: 17 }}
            onClick={forcastHandler}
            className="ml-4 text-slate-900 dark:text-gray-400"
          >
            <BsArrowDownCircle size={25} />
          </motion.button>
        </div>

        <div
          id="slider"
          className="flex lg:mt-8 mt-2 overflow-x-scroll scroll-smooth
         justify-start snap-x overscroll-y-none  mx-6 scrollbar-hide  "
        >
          {forcasts.map((forcs, index) => (
            <ForcastCards key={index} forcs={forcs} />
          ))}
        </div>
        {scroll ? <ForcasrScroll /> : ''}
      </motion.div>
    </>
  );
};

export default Main;
