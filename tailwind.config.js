/** @type {import('tailwindcss').Config} */ 

module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      screen: {
        lg: '1177px',
      },
      fontFamily: {
        nunito: ['Nunito, sans-serif'],
      },
    },
  },
  plugins: [require('tailwind-scrollbar-hide')],
};
