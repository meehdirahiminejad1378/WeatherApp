import React, { useEffect, useState } from 'react';

import cloudRain from '../assets/cloud-rain.svg';
import cloudRainDay from '../assets/cloud-rain-day.svg';
import cloudyDay from '../assets/cloudy-day.svg';
import cloudy from '../assets/cloudy.svg';
import nightRain from '../assets/night-rain.svg';
import snowFall from '../assets/snow-fall.svg';
import sun from '../assets/sun.svg';
import svgCloudy from '../assets/SvgCloudy.svg';
import thunder from '../assets/thunderstorm.svg';
import { BsCloudFog2Fill } from 'react-icons/bs';

const Svg = ({  forcs }) => {
  const [svgi, setSvgi] = useState();
  let icon = forcs?.weather[0].icon;


  useEffect(() => {
    switch (icon) {
      case '01n' || '02n' || '03n' || '04n':
        setSvgi(cloudy);
        break;
      case '01d':
        setSvgi(sun);
        break;

      case '02d' || '04d' || '03d':
        setSvgi(cloudyDay);
        break;

      case '09d' || '10d':
        setSvgi(cloudRainDay);
        break;
      case '10n':
        setSvgi(cloudRain);
        break;
      case '09n':
        setSvgi(nightRain);
        break;
      case '11d' || '11n':
        setSvgi(thunder);
        break;
      case '13d' || '13n':
        setSvgi(snowFall);
        break;
      case '50d' || '50n':
        setSvgi(<BsCloudFog2Fill />);
        break;
      default:
        setSvgi(svgCloudy);
        break;
    }
  }, [icon]);
  return (
    <>
      <div>
        <img src={svgi} alt="icon" className="h-12 w-12" />
      </div>
    </>
  );
};

export default Svg;
